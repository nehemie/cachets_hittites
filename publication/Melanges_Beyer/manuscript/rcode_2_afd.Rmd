```{r}
library(MASS)
```

```{r}
# Extraction des coordonnées avec une ACM 
SiegSymbMCA <- cachets[ , c("Nmbr.Plage.ext", "Dm.ext", 
                            "Dm.int", "Nmbr.hiero", "Nmbr.Symb",
                            "Datation","Orga.hiero","Decor", 
                            "Del.centre", "Surface", "Titre")]


SiegelSymb.mca <- MCA(SiegSymbMCA, 
                      quanti.sup = c(1:5), 
                      quali.sup = c(6), 
                      graph = FALSE)
```

```{r}
# Variable AFD
var.afd <-  c("Nmbr.Plage.ext", "Dm.ext", "Dm.int", "Nmbr.hiero",
             "Nmbr.Symb", "Datation")
# Association Coordonnées ACM avec variables pour AFD
SiMix <- cbind(SiegelSymb.mca$ind$coord[,c(1:2)],
               cachets[,var.afd])

var.afd <- c("Dim 1","Dim 2", var.afd)
# Création de 2 jeux de données: 1) seaux datés  2) datation inconnue
# 1) seaux datés 
SiDat <- SiMix[SiMix$Datation != "Inconnue", var.afd]

# AFD selon la variable datation
SiDat$Datation <- as.factor(as.character(SiDat$Datation))
ldaSiDat <- lda(Datation ~ ., SiDat)

# 2) datation inconnue
SiUnDat <- SiMix[SiMix$Datation == "Inconnue", var.afd]


```


```{r AFD-ggplot2, fig.cap ="Analyse Factorielle Discriminante illustrée selon la datation." }
## Faire un nouveau table avec les données de AFD
lda.m <- rbind(
               cbind(predict(ldaSiDat, 
                             SiUnDat)$x,
                     4), 
               cbind(predict(ldaSiDat)$x, 
                     as.factor(predict(ldaSiDat)$class)
                     ) 
      )

# Ajouter les autres variables
lda.mixte <- merge(SiegSymbMCA, lda.m,  by = "row.names")
rownames(lda.mixte) <- lda.mixte$Row.names

# Data.frame sans datation inconnue pour les ellipses
lda.date <- droplevels.data.frame(lda.mixte[lda.mixte$Datation != "Inconnue",])

 # Calculer coordonnées des ellipses
ellipse_coord <- coord.ellipse(lda.date[,c("Datation","LD1", "LD2")] , 
                               bary = TRUE, 
                               npoint = 1000)
pca_ellipse <- data.frame(ellipse_coord$res)  
colnames(pca_ellipse) <- c("datelli", "dim1elli", "dim2elli")     

# Plot
bwplot <- ggplot(lda.mixte, 
                 aes(x = LD1, 
                     y = LD2,
                     colour = factor(Datation),
                     label = rownames(lda.mixte))
                 )

bwplot + geom_jitter(data = lda.mixte, 
                     aes(x = LD1, y = LD2, 
                         label = rownames(lda.mixte), 
                         shape = factor(Datation), 
                         colour = factor(Datation)), 
                     size = 2) + 
  scale_colour_manual(values = c("#BFBFBF","#A8A8A8","#666666","#000000")) + 
  scale_shape_manual(values = c(4,16,17,15)) +  
#  stat_ellipse(data = lda.date, inherit.aes = FALSE,aes(x = LD1, y = LD2, 
#                         label = rownames(lda.date), 
#                         shape = factor(Datation), 
#                         colour = factor(Datation))) +
  geom_point(data = pca_ellipse, 
             aes(x = dim1elli, y = dim2elli, 
                 colour = factor(datelli), 
                 label = rownames(pca_ellipse)
                 ),size = 0.1) + 
  geom_text(vjust = 1.5, size = 2.5) + 
  scale_fill_grey() + 
  theme_bw() + 
  labs(list(x = labelx, y = labely, title = "LD-Mixte")) + 
  geom_hline(yintercept = 0, linetype = 2) + 
  geom_vline(xintercept = 0, linetype = 2)
```

```{r, fig.cap = "Analyse Factorielle Discriminante illustrée selon le type de surface." }
lda.mixte <- lda.mixte[lda.mixte$Surface != "Etagee",] 

bwplot <- ggplot(lda.mixte, 
                 aes(x = LD1, 
                     y = LD2,
                     colour = factor(Surface),
                     label = rownames(lda.mixte))
                 )

bwplot + geom_jitter(data = lda.mixte, 
                     aes(x = LD1, y = LD2, 
                         label = rownames(lda.mixte), 
                         shape = factor(Surface), 
                         colour = factor(Surface),
                         fill = factor(Surface)), 
                     size = 2) + 
  scale_colour_manual(values = c("#000000","#A8A8A8")) + 
  scale_shape_manual(values = c(18,25)) +  
  stat_ellipse() +
  geom_text(vjust = 1.5, size = 2.5) + 
  scale_fill_grey() + 
  theme_bw() + 
  labs(list(x = labelx, y = labely, title = "LD-Mixte")) +
  geom_hline(yintercept = 0, linetype = 2) + 
  geom_vline(xintercept = 0, linetype = 2)

```

```{r, fig.cap = "Sélection des individus de l’A.F.D. selon les critères (a) présence du symbole L.369 et (b) présence de griffons et/ou d’aigles bicéphales"}
# Union des variables deconcaténées avec lda
lda_deconcat <- merge(lda.m, cachets_deconcat,  by = "row.names")

# Selection deux symboles: L369 et Griffons
lda_l369_greife <- lda_deconcat[lda_deconcat$L369.VITA == "Présence" |
                                lda_deconcat$Greife.o.u.Adler == "Présence"  ,]

lda_l369_greife <- droplevels.data.frame(lda_l369_greife)

bwplot <- ggplot(data = lda_l369_greife, 
                 aes(x = LD1, 
                     y = LD2,
                     label = row.names(lda_l369_greife))
                 ) 

lda_l369 <- lda_deconcat[lda_deconcat$L369.VITA == "Présence",]

# Plot
bwplot + geom_point() + 
  facet_grid(Greife.o.u.Adler ~ L369.VITA)+   
  geom_hline(yintercept = 0, linetype = 2) + 
  geom_vline(xintercept = 0, linetype = 2)
```

