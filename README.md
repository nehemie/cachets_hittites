Cachets Hittites
==============

Research compendium of a study in exploratory analysis on hittite seals.


### Author of this repository

Néhémie Strupler (nehemie.strupler@etu.unistra.fr)


### Publication

Strupler, N. (in print), ‘Dater d’après le cachet’: Une approche méthodologique
pour visualiser les cachets circulaires hittites, in Butterlin P., Patrier J.
and Quenet Ph. (Eds), *Milles et une empreintes. Un Alsacien en Orient. Mélanges
en l'honneur du 65e anniversaire de D. Beyer* (submitted in June 2013).

A preprint of this article is disponible in the [folder publication](publication/Melanges_Beyer/manuscript)


### Data

See folder [data](data/) for further explanation. Collected by [Néhémie
Strupler](mailto:nehemie.strupler@etu.unistra.fr) 



### Presentation

This study was first presented [at the 2nd (french) R-Meeting in Lyon, June
17-28 2013](http://r2013-lyon.sciencesconf.org/18862/). The slideshow is
disponible
[here](https://github.com/nehemie/slideshow/tree/master/2013-06-28--Rchaeology).


#### Undergraduate project

During the meeting in Lyon, [Nathalie
Villa-Vialaneix](http://www.nathalievilla.org/), besides making a lot of nice
and helpful comments on the presentation, proposed to transform the data set in
an undergraduate project for her students, at that time, at the [INSA of
Toulouse, Département Génie Mathématiques et
Modélisation](http://www.math.insa-toulouse.fr/fr/index.html). [Hélène
Lombard](mailto:hpc.lombard@gmail.com) and [Lise
Radoszycki](mailto:radoszyc@etud.insa-toulouse.fr) decided to work on [this
project](Rchaeology-project/).

[The project
(Lombard-Radoszycki-projet.pdf)](Rchaeology-project/Lombard-Radoszycki/Lombard-Radoszycki-projet.pdf)
has been written by [Hélène Lombard](mailto:hpc.lombard@gmail.com) and [Lise
Radoszycki](mailto:radoszyc@etud.insa-toulouse.fr) as undergraduate project
during the academic year 2013-2014 and Néhémie is transforming it in
[Rmarkdown](Rchaeology-project/Lombard-Radoszycki/)

Preview: An html page presenting the project is available at <https://nehemie.gitlab.io/cachets_hittites>
