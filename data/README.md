# README for data

## Raw data

The database was created in German and translated subsequently into French. The
folder [raw](raw/) contains the raw output from the
[database](raw/siegel-raw-2013-07-21.tsv) and the associated script in the
folder were used to create the 'cleaned' and 'deconcatenated'  dataset as well
as their translations.

## Cleaned data and their translation

 - hethitische_siegel_cleaned.tsv
 - hethitische_siegel_deconcat.tsv
 - cachets_hittites_deconcat.tsv
 - cachets_hittites.tsv

### Variables

 - **Annee.decouverte** : Année de découverte 
 - **Numéro d'inventaire** : Numéro de la fouille
 - **ID** : identifiant des  individus selon le format "catalogue - entrée du
   catalogue".
 - **Label** : simplification de l'**ID**.
 - **Dm.ext** : diamètre du cachet en mm.
 - **Dm.int** : diamètre de la plage centrale en mm.
 - **Nmbr.hiero** : nombre de hiéroglyphes différents.
 - **Nmbr.Symb** : nombre de symboles différents.
 - **Nmbre.Plage.ext** : nombre de plages extérieures.
 - **Surface** : type de la surface du cachet. 
 - **Datation** : datation des cachets selon leur contexte de découverte.
 - **Decor** : type de décor de la plage extérieure  (si présente) qui peut
   représenter des animaux, des personnages lors d'une procession ou bien être
purement ornementale.
 - **Del.centre** : délimitation de la plage centrale par une frise ou une
   ligne.
 - **Orga.Hiero** : organisation des hiéroglyphes, classifiée selon les
   catégories :
   + *atypique* : pas d'organisation spécifique.
   + *en doublet* : les hiéroglyphes ordonnés verticalement sont représentés
     symétriquement.
   + *en quinconce* : le premier signe est placé au centre et les hiéroglyphes
     suivants sont répétés aux points cardinaux du premier signe.
   + *T-N-T* : les hiéroglyphes du titre encadrent les hiéroglyphes du nom selon
     le format "Titre-Nom-Titre"(T-N-T).
 - **Origine** : Lieu de découverte du cachet ou de l'impression
 - **Titre** :  titre du propriétaire, soit fonctionnaire (scribe,
   officier, etc.) soit prince ou princesse.
 - **Symb.ext** : liste des différents motifs, séparés par une virgule des
   plages extérieures (frise
   torsadée, aigle et ou griffon, végétaux, rosette, cupule, L.369, L.440,
L.441, L.370-BONUS^2).
 - **Symb.int** : liste des différents motifs, séparés par une virgule de la
   plage centrale
 - **Nom-Hiero** : liste des signes hiéroglyphiques de la légende, séparé
   individuellement par une virgule, selon le format "Catalogue -
Transcription". Les signe sont classifiés selon la liste de Laroche pour les
signes 1 à 497 [^ Laroche 1960], augmentée de la liste de Hawkins[^ Hawkins 1995
: 139]. Les signes de la liste Laroche sont précédés d'un L, ceux identifiés par
Hawkins d'un astérisque. La transcription est en majuscule et en latin s'il
s'agit d'un idéogramme, en minuscule s'il s'agit d'une valeur phonétique.
 - **Titre-Hiero** : liste des signes hiéroglyphiques de chaque titre.  La
liste est redondante par rapport à **Hiero** et est une simple soustraction de
**Symb** soustrait à **Hiero**.
 - **Nom** : lecture des hiéroglyphes du nom.
 - **Nom origine** : langue du nom
 

### Deconcatenated

For the analysis I deconcatenated the symbols and hieroglyphs, with a variable
for each symbols.

## Images

Images of the sealings

