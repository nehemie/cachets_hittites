# Date: 2013-07-21
# Author: Néhémie Strupler
# Licence: CC BY 4.0

# Import packages
library(plyr)

# Read data
Siegel <- read.delim("siegel-raw-2013-07-21.tsv", 
                     na.strings = c(""),
                     encoding = "UTF-8")

# Change coma with point for number
Siegel$DmAussen <- sub(",",".",Siegel$DmAussen)
Siegel$DmAussen  <- as.numeric(as.character(Siegel$DmAussen))
Siegel$DmZentrum <- sub(",",".",Siegel$DmZentrum)
Siegel$DmZentrum <- as.numeric(as.character(Siegel$DmZentrum))

# Classes
Siegel$Fundort <- as.factor(as.character(Siegel$Fundort))
Siegel$ID <- as.character(Siegel$ID)

# Add meaningful col.names (like H.2 instead of Herbordt2005-2)
Siegel$Katalog <- revalue(Siegel$Katalog, c(
            "Alp"   = "A" ,
    "Boehmer1987"   = "B" ,
     "Dincol2008"   = "D" ,
 "Gueterbock1942"   = "G" ,
   "Herbordt2005"   = "H" ,
    "Yoshida1999"   = "Y"  )
       )
rownames(Siegel) <- paste(Siegel$Katalog, Siegel$KatalogNr, sep = ".")

# Revalue 'AussenType'
Siegel$AussenType <- revalue( Siegel$AussenType, c(
            "Hieroglyphen"   = "Hieroglyphen.u.o.Symbole" ,
"Hieroglyphen.und.Symbole"   = "Hieroglyphen.u.o.Symbole" ,
                 "Symbole"   = "Hieroglyphen.u.o.Symbole")
            )
  


# Add symbols together
Siegel$SymbolAussen <- ifelse(!is.na(Siegel$SymbolAussen5),
                          do.call(paste,c(Siegel[,c("SymbolAussen1",
                                                    "SymbolAussen2",
                                                    "SymbolAussen3",
                                                    "SymbolAussen4",
                                                    "SymbolAussen5")],
                                  sep = ", "),
                        ),
                   ifelse(!is.na(Siegel$SymbolAussen4),
                          do.call(paste,c(Siegel[,c("SymbolAussen1",
                                                    "SymbolAussen2",
                                                    "SymbolAussen3",
                                                    "SymbolAussen4")],
                                  sep = ", "),
                        ),     
                   ifelse(!is.na(Siegel$SymbolAussen3),
                          do.call(paste,c(Siegel[,c("SymbolAussen1",
                                                    "SymbolAussen2",
                                                    "SymbolAussen3")],
                                  sep = ", "),
                        ),
                   ifelse(!is.na(Siegel$SymbolAussen2),
                          do.call(paste,c(Siegel[,c("SymbolAussen1",
                                                    "SymbolAussen2")],
                                  sep = ", "),
                        ),
                   ifelse(!is.na(Siegel$SymbolAussen1),
                          paste(Siegel$SymbolAussen1,sep = ", "),
                                  "Abs"))))) 


Siegel$SymbolZentrum <- ifelse(!is.na(Siegel$SymbolZentrum3),
                          do.call(paste,c(Siegel[,c("SymbolZentrum1",
                                                    "SymbolZentrum2",
                                                    "SymbolZentrum3")],
                                  sep = ", "),
                        ),
                   ifelse(!is.na(Siegel$SymbolZentrum2),
                          do.call(paste,c(Siegel[,c("SymbolZentrum1",
                                                    "SymbolZentrum2")],
                                  sep = ", "),
                        ),
                   ifelse(!is.na(Siegel$SymbolZentrum1),
                          paste(Siegel$SymbolZentrum1,sep = ", "),
                                  "Abs")))



Siegel <- 
  Siegel[,
           setdiff(colnames(Siegel),
           c("SymbolAussen1", "SymbolAussen2", "SymbolAussen3",
            "SymbolAussen4", "SymbolAussen5", "SymbolAussen6",
            "SymbolZentrum1", "SymbolZentrum2", "SymbolZentrum3")
  )
          ]


### Add Hieroglyphen together
Siegel$HieroName <- ifelse(!is.na(Siegel$Hieroname5),
                          do.call(paste,c(Siegel[,c("Hieroname1",
                                                    "Hieroname2",
                                                    "Hieroname3",
                                                    "Hieroname4",
                                                    "Hieroname5")],
                                  sep = ", "),
                        ),
                   ifelse(!is.na(Siegel$Hieroname4),
                          do.call(paste,c(Siegel[,c("Hieroname1",
                                                    "Hieroname2",
                                                    "Hieroname3",
                                                    "Hieroname4")],
                                  sep = ", "),
                        ),     
                   ifelse(!is.na(Siegel$Hieroname3),
                          do.call(paste,c(Siegel[,c("Hieroname1",
                                                    "Hieroname2",
                                                    "Hieroname3")],
                                  sep = ", "),
                        ),
                   ifelse(!is.na(Siegel$Hieroname2),
                          do.call(paste,c(Siegel[,c("Hieroname1",
                                                    "Hieroname2")],
                                  sep = ", "),
                        ),
                   ifelse(!is.na(Siegel$Hieroname1),
                          paste(Siegel$Hieroname1,sep = ", "),
                                  "Abs"))))) 


Siegel$HieroTitre <- ifelse(!is.na(Siegel$HieroTitre5),
                          do.call(paste,c(Siegel[,c("HieroTitre1",
                                                    "HieroTitre2",
                                                    "HieroTitre3",
                                                    "HieroTitre4",
                                                    "HieroTitre5")],
                                  sep = ", "),
                        ),
                   ifelse(!is.na(Siegel$HieroTitre4),
                          do.call(paste,c(Siegel[,c("HieroTitre1",
                                                    "HieroTitre2",
                                                    "HieroTitre3",
                                                    "HieroTitre4")],
                                  sep = ", "),
                        ),     
                   ifelse(!is.na(Siegel$HieroTitre3),
                          do.call(paste,c(Siegel[,c("HieroTitre1",
                                                    "HieroTitre2",
                                                    "HieroTitre3")],
                                  sep = ", "),
                        ),
                   ifelse(!is.na(Siegel$HieroTitre2),
                          do.call(paste,c(Siegel[,c("HieroTitre1",
                                                    "HieroTitre2")],
                                  sep = ", "),
                        ),
                   ifelse(!is.na(Siegel$HieroTitre1),
                          paste(Siegel$HieroTitre1,sep = ", "),
                                  "Abs"))))) 


## Delete added columns 
Siegel <- 
  Siegel[, setdiff(colnames(Siegel),
    c("Hieroname1", "Hieroname2", "Hieroname3", "Hieroname4", "Hieroname5",
      "HieroTitre1", "HieroTitre2", "HieroTitre3", "HieroTitre4", "HieroTitre5")
     )
    ]

## Incomplete data (only excluded to avoid NA)
Siegel <- Siegel[Siegel$ID != "Herbordt2005-472",]
Siegel <- Siegel[Siegel$ID != "Boehmer1987-121",] 
Siegel <- Siegel[Siegel$ID != "Yoshida1999-2",] 
Siegel <- Siegel[Siegel$ID != "Yoshida1999-3",]
Siegel <- Siegel[Siegel$ID != "Yoshida1999-3",]
Siegel <- Siegel[Siegel$ID != "Alp-5b",] 
Siegel <- Siegel[Siegel$ID != "Boehmer1987--",] 

Siegel <- subset(Siegel, select = -c(Bemerkung,DatierungVermutet))

##Export
write.table(Siegel, file = "../hethitische_siegel_cleaned.tsv", 
            sep = "\t", 
            col.names = NA, 
            qmethod = "double", 
            fileEncoding = "UTF-8")
