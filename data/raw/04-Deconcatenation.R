Siegel_deconcat <- Siegel[,c(
  "AussenringAnzahl","DmAussen","DmZentrum","VerschiedeneHiero","VerschiedeneSymb",
  "Datierung","Fundort","Anordnung.der.Hieroglyphen",
  "AussenType","BegrenzungZentrum",
  "Wolbung","TitelSiegel",
  "ID", "Inhaber","InhaberOrigin","Jahr","KfNr",
  "SymbolAussen", "SymbolZentrum", "HieroName", "HieroTitre")]

## Add Symb
Siegel_deconcat <- cbind(Siegel_deconcat, Siegel_Symb_count.table[,c(-1,-2,-16)])
Siegel_deconcat <- cbind(Siegel_deconcat,Hierotitre.ct[,-2])

# Convert 0,1,2 in presence / absence
Siegel_deconcat[,22:75] <- as.data.frame(apply(Siegel_deconcat[,22:75], 2, 
                                         function(x) as.factor(x)))

Siegel_deconcat[,22:75]  <- as.data.frame(
  apply(Siegel_deconcat[,22:75] , 2, function(x) gsub("0", "Abwesenheit", x)
        )
  )
Siegel_deconcat[,22:75]  <- as.data.frame(apply(Siegel_deconcat[,22:75] , 2, function(x) 
  gsub("1", "Anwesenheit", x))
  )
Siegel_deconcat[,22:75]  <- as.data.frame(apply(Siegel_deconcat[,22:75] , 2, function(x) 
  gsub("2", "Anwesenheit", x))
  )

write.table(Siegel_deconcat, file = "../hethitische_siegel_deconcat.tsv", 
            sep = "\t", 
            col.names = NA, 
            qmethod = "double", 
            fileEncoding = "UTF-8")

