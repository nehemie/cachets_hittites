# Date: 2013-07-21
# Author: Néhémie Strupler
# Licence: CC BY 4.0"


### Symplifying Symbols
Siegel <- as.data.frame(apply(Siegel, 2, function(x) 
  gsub("Doppelkopfiger.Adler", "Greife.o.u.Adler", x))
  )

Siegel <- as.data.frame(apply(Siegel, 2, function(x) 
  gsub("Griefe.und.doppelkopfiger.Adler", "Greife.o.u.Adler", x))
  )

Siegel <- as.data.frame(apply(Siegel, 2, function(x) 
  gsub("Greife.am.Lebensbaum", "Greife.o.u.Adler", x))
  )

as.numeric <- c("DmAussen","DmZentrum", "AussenringAnzahl",
                "VerschiedeneHiero","VerschiedeneSymb")
Siegel[,as.numeric] <- 
  apply(Siegel[,as.numeric], 2, function(x) as.integer(x))


## On applique la fonction textural aux trois colonnes
# (1=Aussen, 2= Innen, 3=InAus, 4=Rownames)
SymbolAussen.text <- textual(as.data.frame(Siegel$SymbolAussen), 
                             num.text = 1,
                             sep.word = ", ",
                             maj.in.min = FALSE)

SymbolZentrum.text <- textual(as.data.frame(Siegel$SymbolZentrum), 
                              num.text = 1,
                              sep.word = ", ", 
                              maj.in.min = FALSE) 

Symbol.text <- textual(as.data.frame(paste(Siegel$SymbolAussen, Siegel$SymbolZentrum, sep = ", ")),
                       num.text = 1,
                       sep.word = ", ",
                       maj.in.min = FALSE)  




# Select Data for MCA
Siegel_Symb_MCA <- Siegel[,c(
  "AussenringAnzahl","DmAussen","DmZentrum","VerschiedeneHiero","VerschiedeneSymb",
  "Datierung","Fundort","Anordnung.der.Hieroglyphen",
  "AussenType","BegrenzungZentrum", "Darstellung",
  "Wolbung","TitelSiegel")]

## Add Symb
Siegel_Symb_count.table <- Symbol.text$cont.table
Siegel_Symb_MCA <- cbind(Siegel_Symb_MCA, Siegel_Symb_count.table[,c(-1,-16)])

# Convert 0,1,2 in presence / absence
Siegel_Symb_MCA[,14:31] <- as.data.frame(apply(Siegel_Symb_MCA[,14:31], 2, 
                                         function(x) as.factor(x)))

Siegel_Symb_MCA[,14:31]  <- as.data.frame(
  apply(Siegel_Symb_MCA[,14:31] , 2, function(x) gsub("0", "Abwesenheit", x)
        )
  )
Siegel_Symb_MCA[,14:31]  <- as.data.frame(apply(Siegel_Symb_MCA[,14:31] , 2, function(x) 
  gsub("1", "Anwesenheit", x))
  )
Siegel_Symb_MCA[,14:31]  <- as.data.frame(apply(Siegel_Symb_MCA[,14:31] , 2, function(x) 
  gsub("2", "Anwesenheit", x))
  )



# Multiple Correspondance Analysis with Symbols
Siegel_Symb.MCA <- 
  MCA(Siegel_Symb_MCA, quanti.sup = c(1:5), quali.sup = c(14:31))

plot(Siegel_Symb.MCA, invisible = c("var","quali.sup"), cex = 0.7)
plot(Siegel_Symb.MCA, invisible = c("var","quali.sup"), cex = 0.7, habillage = 6)
plot(Siegel_Symb.MCA, invisible = c("ind"), cex = 0.7)


## Description
Siegel_Symb.MCA
Siegel_Symb.MCA$eig


plotellipses(Siegel_Symb.MCA,
             keepvar = c("Datierung","Anordnung.der.Hieroglyphen",
                         "AussenType","BegrenzungZentrum"))

plotellipses(Siegel_Symb.MCA,keepvar = c("Datierung","Darstellung",
                                         "Wolbung","TitelSiegel"))

plotellipses(Siegel_Symb.MCA,keepvar = c("L369-VITA","L370-BONUS2",
                                         "L440-VITA","L441-VITA"))

plotellipses(Siegel_Symb.MCA,keepvar = c("Datierung","Kugel",
                                         "Flechtband","Rosette"))




############ MFA Symbol

Siegel_Symb.mfa <- MFA(Siegel_Symb_MCA, 
                     group = c(5,2,6,18),
                     type = c("s","n","n","n"),
                     name.group = c("Quanti","DatFund","Quali","Symb"),
                     num.group.sup = c(2))

summary(Siegel_Symb.mfa )
plot(Siegel_Symb.mfa , invisible = "quali",habillage = 6)
plot(Siegel_Symb.mfa , invisible = "ind")

plotellipses(Siegel_Symb.mfa ,
              keepvar = c("Datierung","Anordnung.der.Hieroglyphen","AussenType","BegrenzungZentrum"))
plotellipses(Siegel_Symb.mfa ,
              keepvar = c("Datierung","Darstellung","Wolbung","TitelSiegel"))
plotellipses(Siegel_Symb.mfa ,
              keepvar = c("L369-VITA","L370-BONUS2","L440-VITA","L441-VITA"))


plot(Siegel_Symb.mfa, 
     invisible = "quali", 
     habillage = "group",
     cex = 0.7, 
     partial = c("H.124", "H.136", "H.297", "H.323")
     )

