```{r 'load libraries', message=FALSE}
library(knitr)
library(FactoMineR)
library(ggplot2)
```

```{r}
# Lire les données
cachets_tsv <- "../../data/hethitische_siegel_cleaned.tsv"
cachets_deconcat_tsv <- "../../data/hethitische_siegel_deconcat.tsv"
Siegel <- read.delim(cachets_tsv, row.names = 1)
Siegel_deconcat <- read.delim(cachets_deconcat_tsv, row.names = 1)
```


```{r, message=FALSE, warning=FALSE}
# Analyse en Composantes Principales (ACP)

pca.var <- c("AussenringAnzahl","DmAussen","DmZentrum","VerschiedeneHiero",
             "VerschiedeneSymb", "Datierung","Anordnung.der.Hieroglyphen",
             "AussenType", "BegrenzungZentrum" , "Darstellung","Wolbung",
             "ErhaltungsDm","TitelSiegel")

sieg.pca <- PCA(Siegel[,pca.var],
                quali.sup = c(6:13), 
                graph = F)

```

```{r ACP-plot, fig.cap = "Analyse en Composantes Principales illustrée selon la datation"}
# Coordonnées des ellipses
concat.data <- cbind.data.frame(Siegel[,c("Datierung")],sieg.pca$ind$coord)
ellipse.coord <- coord.ellipse(concat.data, bary = TRUE, npoint = 1000)
ellipse.coord$res <- ellipse.coord$res[1001:4000,] 
pca.ellipse <- data.frame(ellipse.coord$res)  
colnames(pca.ellipse) <- c("datelli","dim1elli","dim2elli")

# Coordonnées des individus
pca.gg <- sieg.pca$ind$coord[,c(1:2)]
pca.gg <- data.frame(pca.gg)
df.indi <- cbind(pca.gg,Siegel$Datierung)
colnames(df.indi) <- c("Dim1","Dim2","Dat")

# Label de la 1ère dimension
labelx <- round(sieg.pca$eig[1,2],1)
labelx <- paste("Dimension 1 (",labelx,"%)", sep = "")

# Label de la 2ème dimension
labely <- round(sieg.pca$eig[2,2],1)
labely <- paste("Dimension 2 (",labely,"%)", sep = "")

# Base de ggplot
bwplot <- ggplot(df.indi, aes(x = Dim1 ,
                    y = Dim2, 
                    label = rownames(df.indi)))


# Plot en gris
bwplot + geom_jitter(data = df.indi, 
                     aes(x = Dim1,
                         y = Dim2, 
                         label = rownames(df.indi),  
                         shape = factor(Dat), 
                         colour = factor(Dat)),
                     size = 2) + 
  scale_colour_manual(values = c("#000000","#A8A8A8","#666666","#000000")) + 
  scale_shape_manual(values = c(4,16,17,15)) + geom_text(vjust = 2, size = 3) +
#  stat_ellipse(data = pca.ellipse, 
#             aes(x = dim1elli, y = dim2elli, 
#                 colour = factor(datelli), 
#                 label = rownames(pca.ellipse)
#                 )) +  
  geom_point(data = pca.ellipse, 
             aes(x = dim1elli, y = dim2elli, 
                 colour = factor(datelli), 
                 label = rownames(pca.ellipse)
                 ),
             size = 0.1) + 
  
  scale_fill_grey() + 
  theme_bw() + 
  labs(x = labelx, y = labely, title = "Analyse en Composantes Principales") +   
  geom_hline(yintercept = 0, linetype = 2) + 
  geom_vline(xintercept = 0, linetype = 2)  
```
